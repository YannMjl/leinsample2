(defproject io.example-yann/sample2lein "0.1.0-SNAPSHOT"
  :description "This is sample 2 of a clojure project"
  :url "http://example.com/clojureproject2"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [clj-x256 "0.0.1"]
                 [io.example-yann/sample1lein "0.1.0-SNAPSHOT" :extend "pom"]]
  :main sample2lein.corelein
  :oat [sample2lein.core]
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}}
  :plugins [[org.apache.maven.wagon/wagon-http "3.0.0"]]
  :signing {:gpg-key "yannmjl@chenpo.io"}
  :repositories [["snapshot" {:url "http://examples-yann.mycloudrepo.localhost:6123/repositories/lein-snapshot"
                              :creds :gpg}]])

  (require 'cemerick.pomegranate.aether)
        (cemerick.pomegranate.aether/register-wagon-factory!
                              "http" #(org.apache.maven.wagon.providers.http.HttpWagon.))
